#!/bin/sh

export PS1="[flatpak-env] $PS1"

echo "===================================================================="
echo "              Entering Flatpak build environment                    "
echo "                                                                    "
echo "                   BATTLECRUISER OPERATIONAL                        "
echo "                          >(°)__/                                   "
echo "                           (_~_/                                    "
echo "                         ~~~~~~~~~~~~                               "
echo "===================================================================="

bash 
