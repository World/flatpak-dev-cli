#!/bin/bash

# Script which must be sourced to setup the development environment.

# This has to be the first command because BASH_SOURCE[0] gets changed.
SCRIPT=${BASH_SOURCE[0]:-$0}

[[ "${BASH_SOURCE[0]}" == "$0" ]] \
    && echo "This script should not be executed but sourced like:" \
    && echo "    $ source $0" \
    && echo \
    && return 1

if [ $# -lt 2 ]
  then
    echo "Missing arguments, expected three arguments in this order: \$development-prefix \$source-path \$manifest-path"
    return 1
fi

SCRIPT_DIR=$(dirname $(realpath $SCRIPT))
export FLATPAK_APP_PATH=$(realpath $1)
export FLATPAK_MANIFEST_PATH=$(realpath $2)

# Use flatpakenv for running commands in the sandbox.
alias flatpakenv="$SCRIPT_DIR/flatpak-dev.py -d"

echo "-> Setting up environment if needed..."

# Enter the flatpak build environment
$SCRIPT_DIR/flatpak-dev.py -d
